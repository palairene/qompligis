# Funding

```{admonition} Ready to contribute?
Plugin is free to use, not to develop. If you use it quite intensively or are interested in improving it, please consider to contribute to the code, to the documentation or fund some developments:

- [identified enhancements](https://gitlab.com/Oslandia/qgis/qompligis/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=enhancement)
- want to fund? Please, [send us an email](mailto:qgis@oslandia.com)
```

## Sponsors

Thanks to the sponsors:

```{eval-rst}
.. panels::
    :card: shadow
    :container: container-lg pb-4
    :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2
    :img-top-cls: pl-5 pr-5

    ---
    :img-top: ../../QompliGIS/resources/images/logo_megeve.jpg

    Megève
    ^^^^^^

    The commune funded the initial development.

    ++++++
    .. link-button:: https://mairie.megeve.fr/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block
```
