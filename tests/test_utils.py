"""
test utils
"""
from collections import Counter
from pathlib import Path

import pytest
import yaml
from qgis.core import (
    QgsFeature,
    QgsField,
    QgsFields,
    QgsGeometry,
    QgsSpatialIndex,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QVariant

from QompliGIS.report import ReportFormat
from QompliGIS.utils import (
    DoVerif,
    FieldsComparison,
    FieldsOrder,
    TopologyRules,
    compare_fields,
    crs_compare,
    flat_geometry_type_compare,
    geometry_type_compare,
    get_input_format,
    has_curved_segments,
    has_holes,
    has_minimal_area,
    has_minimal_length,
    has_topology_errors,
    is_valid,
    is_null,
    is_empty,
    list_dxf_info,
    list_dxf_layers,
    list_dxf_vlayers,
    list_gpkg_info,
    list_gpkg_layers,
    list_gpkg_vlayers,
    list_shp_files,
    list_shp_info,
    list_shp_vlayers,
)


def _list_to_fields(field_list):
    """_list_to_fields.

    :param field_list:
    """
    fields = QgsFields()
    for field in field_list:
        fields.append(field)
    return fields


def test_crs_same():
    """test_crs_same."""
    origin_layer = QgsVectorLayer("PointZM?crs=EPSG:3946", "pointzm", "memory")
    compare_layer = QgsVectorLayer("PointZM?crs=EPSG:3946", "pointzm", "memory")

    result = crs_compare(origin_layer, compare_layer)
    expected = (True, "EPSG:3946", "EPSG:3946")
    assert result == expected


def test_crs_same_type_differ():
    """test_crs_same_type_differ."""
    origin_layer = QgsVectorLayer("PointZM?crs=EPSG:3946", "pointzm", "memory")
    compare_layer = QgsVectorLayer("Point?crs=EPSG:3946", "point", "memory")

    result = crs_compare(origin_layer, compare_layer)
    expected = (True, "EPSG:3946", "EPSG:3946")
    assert result == expected


def test_crs_differ():
    """test_crs_differ."""
    origin_layer = QgsVectorLayer("PointZM?crs=EPSG:3946", "pointzm", "memory")
    compare_layer = QgsVectorLayer("Point?crs=EPSG:3945", "point", "memory")

    result = crs_compare(origin_layer, compare_layer)
    expected = (False, "EPSG:3946", "EPSG:3945")
    assert result == expected


def test_same_fields_same_order():
    """test_same_fields_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])

    assert (True, {}) == compare_fields(fields, fields)


def test_same_fields_order_differ():
    """test_same_fields_order_differ."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field3, field2])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.ORDERDIFFER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.SAMEFIELD,),
            },
        },
    )
    assert result == expected


def test_missing_one_field_same_order():
    """test_missing_one_field_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.FIELDSDIFFER, Counter({"diam": -1})),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.NOTPRESENT,),
            },
        },
    )
    assert result == expected


def test_precision_differ_same_name_same_order():
    """test_precision_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Double, "double", len=10, prec=4)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.PRECISIONDIFFER, (6, 4)),
            },
        },
    )
    assert result == expected


def test_length_differ_same_name_same_order():
    """test_length_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Double, "double", len=11, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.LENGTHDIFFER, (10, 11)),
            },
        },
    )
    assert result == expected


def test_type_differ_same_name_same_order():
    """test_type_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Int, "integer", len=11)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (
                    FieldsComparison.TYPEDIFFER,
                    (("double", 10, 6), ("integer", 11, 0)),
                ),
            },
        },
    )
    assert result == expected


def test_subtype_differ_same_name_same_order():
    """test_subtype_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField(
        "diam", QVariant.Double, "double", len=10, prec=6, subType=QVariant.Int
    )

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (
                    FieldsComparison.OTHERDIFFERENCE,
                    (("double", 10, 6), ("double", 10, 6)),
                ),
            },
        },
    )
    assert result == expected


def test_geometry_type_same():
    """test_geometry_type_same."""
    origin_layer = QgsVectorLayer("PointZM", "pointzm", "memory")
    compare_layer = QgsVectorLayer("PointZM", "pointzm", "memory")

    result = geometry_type_compare(origin_layer, compare_layer)
    expected = (True, "PointZM", "PointZM")
    assert result == expected

    result = flat_geometry_type_compare(origin_layer, compare_layer)
    expected = (True, "Point", "Point")
    assert result == expected


def test_geometry_type_flat_ok():
    """test_geometry_type_flat_ok."""
    origin_layer = QgsVectorLayer("PointZM", "pointzm", "memory")
    compare_layer = QgsVectorLayer("PointZ", "pointzm", "memory")

    result = geometry_type_compare(origin_layer, compare_layer)
    expected = (False, "PointZM", "PointZ")
    assert result == expected

    result = flat_geometry_type_compare(origin_layer, compare_layer)
    expected = (True, "Point", "Point")
    assert result == expected


def test_geometry_type_diff():
    """test_geometry_type_diff."""
    origin_layer = QgsVectorLayer("Polygon", "pointzm", "memory")
    compare_layer = QgsVectorLayer("Point", "pointzm", "memory")

    result = geometry_type_compare(origin_layer, compare_layer)
    expected = (False, "Polygon", "Point")
    assert result == expected

    result = flat_geometry_type_compare(origin_layer, compare_layer)
    expected = (False, "Polygon", "Point")
    assert result == expected


def test_is_valid():
    """test_is_valid."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 1 1, 1 0, 0 1, 0 0))")
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    result = is_valid(absgeom)
    assert result is False

    geom = QgsGeometry.fromWkt("Polygon((0 0, 1 0, 1 1, 0 1, 0 0))")
    absgeom = geom.constGet()

    result = is_valid(absgeom)
    assert result is True

    geom = QgsGeometry.fromWkt("Polygon EMPTY")
    absgeom = geom.constGet()

    result = is_valid(absgeom)
    assert result is False


def test_is_null():
    """test_is_null."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 1 1, 1 0, 0 1, 0 0))")

    result = is_null(geom)
    assert result is False

    geom = QgsGeometry.fromWkt("")

    result = is_null(geom)
    assert result is True


def test_is_empty():
    """test_is_empty."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 1 0, 1 1, 0 1, 0 0))")

    result = is_empty(geom)
    assert result is False

    geom = QgsGeometry.fromWkt("Polygon()")

    result = is_empty(geom)
    assert result is True


def test_has_minimal_area():
    """test_has_minimal_area."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 10 0, 10 10, 0 10, 0 0))")

    assert has_minimal_area(geom, 5) is True
    assert has_minimal_area(geom, 500) is False


def test_has_minimal_length():
    """test_has_minimal_length."""
    geom = QgsGeometry.fromWkt("LineString(0 0, 10 0, 10 10, 0 10, 0 0)")

    assert has_minimal_length(geom, 5) is True
    assert has_minimal_length(geom, 500) is False


def test_has_holes():
    """test_has_holes."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 10 0, 10 10, 0 10, 0 0))")
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_holes(absgeom) is False

    geom = QgsGeometry.fromWkt(
        """Polygon((0 0, 10 0, 10 10, 0 10, 0 0),
                               (5 5, 6 5, 6 6, 5 6, 5 5))"""
    )
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_holes(absgeom) is True

    geom = QgsGeometry.fromWkt(
        "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)),((20 35, 10 30, 10 10, 30 5, 45 20, 20 35))"
    )
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_holes(absgeom) is False

    geom = QgsGeometry.fromWkt(
        "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)),((20 35, 10 30, 10 10, 30 5, 45 20, 20 35),(30 20, 20 15, 20 25, 30 20) ))"
    )
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_holes(absgeom) is True


def test_has_curved_segments():
    """test_has_curved_segments."""
    geom = QgsGeometry.fromWkt("Polygon((0 0, 10 0, 10 10, 0 10, 0 0))")
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_curved_segments(absgeom) is False

    geom = QgsGeometry.fromWkt(
        """CurvePolygon (CompoundCurve (
                               CircularString (-1 0, 0 1, 1 0, 0 -1, -1 0) ) )"""
    )
    absgeom = geom.constGet()  # get QgsAbstractGeometry

    assert has_curved_segments(absgeom) is True


def test_has_topology_errors():
    """test_has_topology_errors."""
    layer = QgsVectorLayer("MultiPolygon", "origin_layer_polygon", "memory")
    layer_provider = layer.dataProvider()
    feature_one = QgsFeature()
    feature_one.setGeometry(
        QgsGeometry.fromWkt("Polygon((0 0, 10 0, 10 10, 0 10, 0 0))")
    )
    feature_two = QgsFeature()
    feature_two.setGeometry(
        QgsGeometry.fromWkt("Polygon((20 0, 30 0, 30 30, 20 20, 20 0))")
    )
    feature_three = feature_two
    feature_four = QgsFeature()
    feature_four.setGeometry(
        QgsGeometry.fromWkt("Polygon((5 0, 20 0, 20 20, 5 5, 5 0))")
    )
    layer_provider.addFeatures([feature_one, feature_two, feature_three, feature_four])
    index = QgsSpatialIndex(layer.getFeatures())
    features = list(layer.getFeatures())

    # Verification of duplicates
    assert (
        has_topology_errors(
            layer,
            index,
            features[0].id(),
            features[0].geometry(),
            rule=TopologyRules.DUPLICATES,
        )
    ) == []
    assert (
        has_topology_errors(
            layer,
            index,
            features[2].id(),
            features[2].geometry(),
            rule=TopologyRules.DUPLICATES,
        )
    ) == ["2"]

    # Verification of overlaps
    assert (
        has_topology_errors(
            layer,
            index,
            features[1].id(),
            features[1].geometry(),
            rule=TopologyRules.OVERLAPS,
        )
    ) == []
    assert (
        has_topology_errors(
            layer,
            index,
            features[0].id(),
            features[0].geometry(),
            rule=TopologyRules.OVERLAPS,
        )
    ) == ["4"]


def test_same_fields_same_order():
    """test_same_fields_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])

    assert (True, {}) == compare_fields(fields, fields)


def test_same_fields_order_differ():
    """test_same_fields_order_differ."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field3, field2])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.ORDERDIFFER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.SAMEFIELD,),
            },
        },
    )
    assert result == expected


def test_missing_one_field_same_order():
    """test_missing_one_field_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.FIELDSDIFFER, Counter({"diam": -1})),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.NOTPRESENT,),
            },
        },
    )
    assert result == expected


def test_precision_differ_same_name_same_order():
    """test_missing_one_field_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Double, "double", len=10, prec=4)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.PRECISIONDIFFER, (6, 4)),
            },
        },
    )
    assert result == expected


def test_length_differ_same_name_same_order():
    """test_length_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Double, "double", len=11, prec=6)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (FieldsComparison.LENGTHDIFFER, (10, 11)),
            },
        },
    )
    assert result == expected


def test_type_differ_same_name_same_order():
    """test_type_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField("diam", QVariant.Int, "integer", len=11)

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (
                    FieldsComparison.TYPEDIFFER,
                    (("double", 10, 6), ("integer", 11, 0)),
                ),
            },
        },
    )
    assert result == expected


def test_subtype_differ_same_name_same_order():
    """test_subtype_differ_same_name_same_order."""
    field1 = QgsField("fid", QVariant.Int, "integer", len=10)
    field2 = QgsField("name", QVariant.String, "string", len=10)
    field3 = QgsField("diam", QVariant.Double, "double", len=10, prec=6)
    field_diff = QgsField(
        "diam", QVariant.Double, "double", len=10, prec=6, subType=QVariant.Int
    )

    fields = _list_to_fields([field1, field2, field3])
    fields_comp = _list_to_fields([field1, field2, field_diff])

    result = compare_fields(fields, fields_comp)
    expected = (
        False,
        {
            "fieldsOrder": (FieldsOrder.SAMEORDER, Counter()),
            "fieldsComparison": {
                "fid": (FieldsComparison.SAMEFIELD,),
                "name": (FieldsComparison.SAMEFIELD,),
                "diam": (
                    FieldsComparison.OTHERDIFFERENCE,
                    (("double", 10, 6), ("double", 10, 6)),
                ),
            },
        },
    )
    assert result == expected


def test_list_dxf_info():
    """test_list_dxf_info."""
    assert list_dxf_info(
        str(Path(__file__).resolve().parent / "testdata" / "dxf" / "source.dxf")
    ) == {
        "autre_ouvrage": ("Point", []),
        "cana": ("LineString", []),
        "branchement": ("LineString", []),
    }


def test_list_dxf_layers():
    """test_list_dxf_layers."""
    assert list_dxf_layers(
        str(Path(__file__).resolve().parent / "testdata" / "dxf" / "source.dxf")
    ) == ["autre_ouvrage", "branchement", "cana"]


def test_list_dxf_vlayers():
    """test_list_dxf_vlayers."""
    vlayers = list_dxf_vlayers(
        str(Path(__file__).resolve().parent / "testdata" / "dxf" / "source.dxf")
    )
    ref_names = ["autre_ouvrage", "branchement", "cana"]
    names = []
    for vlayer in vlayers:
        names.append(vlayer.name())
    assert ref_names == sorted(names)


def test_list_shp_info():
    """test_list_shp_info."""
    info = list_shp_info(
        str(Path(__file__).resolve().parent / "testdata" / "shp" / "source")
    )
    assert info["autre_ouvrage"][0] == "Point"
    assert ["angle", "id", "id_noeud", "lat", "lon"] == sorted(
        [a.name() for a in info["autre_ouvrage"][1]]
    )
    assert info["branchement"][0] == "MultiLineString"
    assert ["id", "id_canalis", "raepa_diam", "raepa_long", "raepa_mate"] == sorted(
        [a.name() for a in info["branchement"][1]]
    )
    assert info["cana"][0] == "MultiLineString"
    assert ["id", "id_canalis", "status"] == sorted([a.name() for a in info["cana"][1]])


def test_list_shp_files():
    """test_list_shp_files."""
    assert ["autre_ouvrage", "branchement", "cana"] == list_shp_files(
        str(Path(__file__).resolve().parent / "testdata" / "shp" / "source")
    )


def test_list_shp_vlayers():
    """test_list_shp_vlayers."""
    vlayers = list_shp_vlayers(
        str(Path(__file__).resolve().parent / "testdata" / "shp" / "source")
    )
    ref_names = ["autre_ouvrage", "branchement", "cana"]
    names = []
    for vlayer in vlayers:
        names.append(vlayer.name())
    assert ref_names == sorted(names)


def test_list_gpkg_info():
    """test_list_gpkg_info."""
    info = list_gpkg_info(
        str(Path(__file__).resolve().parent / "testdata" / "gpkg" / "source.gpkg")
    )
    assert info["autre_ouvrage"][0] == "Point"
    assert [
        "angle",
        "bg_comment",
        "bg_commune",
        "fid",
        "id_noeud",
        "lat",
        "lon",
        "raepa_alti",
        "raepa_ande",
        "raepa_anfi",
        "raepa_etat",
        "raepa_expl",
        "raepa_mait",
        "raepa_nom_",
        "raepa_type",
        "source",
    ] == sorted([a.name() for a in info["autre_ouvrage"][1]])
    assert info["branchement"][0] == "LineString"
    assert [
        "bg_adresse",
        "bg_comment",
        "bg_commune",
        "bg_compteu",
        "bg_date_po",
        "bg_libelle",
        "bg_nom_sec",
        "bg_num_par",
        "bg_robinet",
        "fid",
        "id_canalis",
        "onema_date",
        "onema_dern",
        "onema_loca",
        "onema_tech",
        "onema_type",
        "raepa_alti",
        "raepa_ande",
        "raepa_anfi",
        "raepa_bran",
        "raepa_cat_",
        "raepa_diam",
        "raepa_en_s",
        "raepa_etat",
        "raepa_expl",
        "raepa_fonc",
        "raepa_long",
        "raepa_mait",
        "raepa_mate",
        "raepa_mode",
        "raepa_nom_",
    ] == sorted([a.name() for a in info["branchement"][1]])
    assert info["cana"][0] == "MultiLineString"
    assert [
        "bg_adresse",
        "bg_alti_de",
        "bg_alti_fi",
        "bg_annee_r",
        "bg_comment",
        "bg_commune",
        "bg_libelle",
        "bg_nom_sec",
        "bg_num_par",
        "bg_positio",
        "bg_pressio",
        "bg_profond",
        "bg_rugosit",
        "bg_secteur",
        "bg_sens",
        "bg_sensibl",
        "bg_type",
        "fid",
        "id_canalis",
        "id_noeud_1",
        "id_noeud_a",
        "onema_caus",
        "onema_corr",
        "onema_cote",
        "onema_da_1",
        "onema_date",
        "onema_depo",
        "onema_env_",
        "onema_etat",
        "onema_fabr",
        "onema_join",
        "onema_lit_",
        "onema_pr_1",
        "onema_pr_2",
        "onema_pr_3",
        "onema_prec",
        "onema_prot",
        "onema_tech",
        "onema_traf",
        "onema_type",
        "onema_vale",
        "onema_vali",
        "raepa_alti",
        "raepa_ande",
        "raepa_anfi",
        "raepa_bran",
        "raepa_cat_",
        "raepa_diam",
        "raepa_en_s",
        "raepa_etat",
        "raepa_expl",
        "raepa_fonc",
        "raepa_long",
        "raepa_mait",
        "raepa_mate",
        "raepa_mode",
        "status",
    ] == sorted([a.name() for a in info["cana"][1]])


def test_list_gpkg_vlayers():
    """test_list_gpkg_vlayers."""
    vlayers = list_gpkg_vlayers(
        str(Path(__file__).resolve().parent / "testdata" / "gpkg" / "source.gpkg")
    )
    ref_names = ["autre_ouvrage", "branchement", "cana"]
    names = []
    for vlayer in vlayers:
        names.append(vlayer.name())
    assert ref_names == sorted(names)


def test_list_gpkg_layers():
    """test_list_gpkg_layers."""
    assert list_gpkg_layers(
        str(Path(__file__).resolve().parent / "testdata" / "gpkg" / "source.gpkg")
    ) == ["autre_ouvrage", "branchement", "cana"]


def test_DoVerif():
    """test_DoVerif."""

    # Verif of dxf must must not crash but compliance is bad
    test_path = Path(__file__).resolve().parent / "testdata"
    conf_dict = yaml.full_load(
        (test_path / "test_verif_dxf.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}",
            str(test_path / "dxf" / "tocompare.dxf"),
        )
    )
    verif = DoVerif(conf_dict, str(test_path / "dxf" / "source.dxf"))
    assert verif.run(), "The verification must not fail"
    assert not verif.run_result, "The compliance must fail"

    # Verif of geopackages must not crash but compliance is bad
    conf_dict = yaml.full_load(
        (test_path / "test_verif_gpkg.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}",
            str(test_path / "gpkg" / "tocompare.gpkg"),
        )
    )
    verif = DoVerif(conf_dict, str(test_path / "gpkg" / "source.gpkg"))
    assert verif.run(), "The verification must not fail"
    assert not verif.run_result, "The compliance must fail"

    # Verif of shapefiles must not crash but compliance is bad
    conf_dict = yaml.full_load(
        (test_path / "test_verif_shp.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}",
            str(test_path / "shp" / "tocompare"),
        )
    )
    verif = DoVerif(conf_dict, str(test_path / "shp" / "source"))
    assert verif.run(), "The verification must not fail"
    assert not verif.run_result, "The compliance must fail"

    # A verification between different formats must fail
    conf_dict = yaml.full_load(
        (test_path / "test_verif_dxf.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}",
            str(test_path / "shp" / "tocompare"),
        )
    )
    verif = DoVerif(conf_dict, str(test_path / "shp" / "source"))
    with pytest.raises(ValueError) as tt:
        verif.run()
    assert (
        str(tt.value)
        == "The data format to check (Shapefiles) does not correspond with the reference data (DAO (*.dxf))"
    )
