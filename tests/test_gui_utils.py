"""
Test for gui_utils
"""
from pathlib import Path

from qgis.gui import QgsFileWidget
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QComboBox, QLineEdit, QMessageBox, QWizard

from QompliGIS import gui_utils, utils
from QompliGIS.gui_utils import (
    CheckComplianceWizard,
    ConfigEditPage,
    ConfigInputPage,
    ConfigPage,
    DataInputPage,
    SaveConfigPage,
    SaveReportPage,
    VerifDataInputPage,
    VerifPage,
)
from QompliGIS.report import ReportFormat
from QompliGIS.utils import InputFormat


# pylint: disable=C0103
def test_DataInputPage(qtbot, mocker):
    """test DataInputPage.

    :param qtbot:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")

    dip = DataInputPage()
    wiz = QWizard()
    wiz.addPage(dip)
    cbx = dip.findChildren(QComboBox)[0]
    qtbot.addWidget(wiz)

    assert cbx.itemText(0) == ""
    assert cbx.itemText(1) == "Shapefiles"
    assert cbx.itemText(2) == "DAO (*.dxf)"
    assert cbx.itemText(3) == "Geopackage (*.gpkg)"

    filepath = dip.findChildren(QLineEdit)[0]

    qtbot.keyClicks(filepath, "no_name")

    assert not dip.validatePage()
    message_box.warning.assert_called_once_with(
        None, "Warning", "The path no_name does not exist!"
    )

    filepath.clear()
    qtbot.keyClicks(filepath, __file__)
    assert dip.validatePage()


def test_VerifDataInputPage(qtbot, mocker):
    """test VerifDataInputPage.

    :param qtbot:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")

    # Verify the format selection according to the input data format
    # SHAPEFILES -> select a folder
    mocker.patch(
        "QompliGIS.gui_utils.get_input_format", return_value=InputFormat.SHAPEFILES
    )
    config_file = QLineEdit("yoyo")
    vdip = VerifDataInputPage()
    vdip.registerField("config_file", config_file)
    wiz = QWizard()
    wiz.addPage(vdip)
    qtbot.addWidget(wiz)
    wiz.show()
    assert vdip.filepath.storageMode() == QgsFileWidget.StorageMode.GetDirectory

    # GEOPACKAGE -> select a file
    mocker.patch(
        "QompliGIS.gui_utils.get_input_format", return_value=InputFormat.GEOPACKAGE
    )
    vdip = VerifDataInputPage()
    vdip.registerField("config_file", config_file)
    wiz = QWizard()
    wiz.addPage(vdip)
    qtbot.addWidget(wiz)
    wiz.show()
    assert vdip.filepath.storageMode() == QgsFileWidget.StorageMode.GetFile

    qtbot.keyClicks(vdip.filepath.lineEdit(), __file__)
    assert vdip.validatePage()
    vdip.filepath.lineEdit().clear()

    qtbot.keyClicks(vdip.filepath.lineEdit(), __file__[:-2])
    assert not vdip.validatePage()
    message_box.warning.assert_called_once_with(
        None, "Warning", "The path " + __file__[:-2] + " does not exist!"
    )


def test_ConfigInputPage(qtbot, mocker):
    """test ConfigInputPage.

    :param qtbot:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")

    cip = ConfigInputPage()
    wiz = QWizard()
    wiz.addPage(cip)
    qtbot.addWidget(wiz)
    wiz.show()
    file_widget = cip.findChildren(QgsFileWidget)[0]

    # Everything is good
    mocker.patch("QompliGIS.gui_utils.check_conf_dict_format", return_value="")
    mocker.patch(
        "QompliGIS.gui_utils.yaml.full_load", return_value={"filepath": __file__}
    )
    qtbot.keyClicks(file_widget.lineEdit(), __file__)
    assert cip.validatePage()
    file_widget.lineEdit().clear()

    # YAML is good but reference data not accessible
    mocker.patch(
        "QompliGIS.gui_utils.yaml.full_load", return_value={"filepath": __file__[:-2]}
    )
    qtbot.keyClicks(file_widget.lineEdit(), __file__)
    assert not cip.validatePage()
    message_box.critical.assert_called_once_with(
        None,
        "Error",
        "The path "
        + __file__[:-2]
        + " referenced in the config file does not exist!\nDo you want to edit the path of the file/folder?",
        buttons=message_box.Yes | message_box.No,
    )
    file_widget.lineEdit().clear()

    # YAML is corrupted
    mocker.patch("QompliGIS.gui_utils.check_conf_dict_format", return_value="bad")
    qtbot.keyClicks(file_widget.lineEdit(), __file__)
    assert not cip.validatePage()
    message_box.warning.assert_called_once_with(None, "Bad configuration file!", "bad")
    file_widget.lineEdit().clear()

    # Config file does not exist
    qtbot.keyClicks(file_widget.lineEdit(), __file__[:-2])
    assert not cip.validatePage()
    message_box.warning.assert_called_with(
        None, "Warning", "The configuration file " + __file__[:-2] + " does not exist!"
    )


def test_ConfigPage(qtbot, mocker, tmp_path):
    """test ConfigPage.

    :param qtbot:
    :param mocker:
    :param tmp_path:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")

    input_format = QComboBox()
    input_format.addItems(["", "Shapefiles", "", ""])
    input_format.setCurrentIndex(1)  # 1 = shapefile
    filepath = QLineEdit(str(Path(__file__).parent / "testdata" / "shp" / "source"))
    save_path = QLineEdit(str(tmp_path / "test.yaml"))

    cp = ConfigPage()
    cp.registerField(
        "input_format", input_format, "currentText", input_format.currentTextChanged
    )
    cp.registerField("filepath", filepath)
    cp.registerField("save_path", save_path)
    wiz = QWizard()
    wiz.addPage(cp)
    qtbot.addWidget(wiz)
    wiz.show()
    cp.layers_view.setCurrentRow(0)
    cp.stack.currentWidget().tbl_fields.item(2, 1).setCheckState(Qt.Checked)
    cp.layers_view.setCurrentRow(1)
    cp.stack.currentWidget().tbl_fields.item(2, 1).setCheckState(Qt.Checked)
    cp.layers_view.setCurrentRow(2)
    cp.stack.currentWidget().tbl_fields.item(2, 1).setCheckState(Qt.Checked)
    qtbot.keyClick(cp.stack.currentWidget().spb_min_length, "a", Qt.ControlModifier)
    qtbot.keyClick(cp.stack.currentWidget().spb_min_length, Qt.Key_Delete)
    qtbot.keyClicks(cp.stack.currentWidget().spb_min_length, "336.7")
    cp.exportConfig()
    message_box.information.assert_called_once_with(
        None,
        "Completed",
        "The configuration file has been saved to " + save_path.text(),
    )
    tmp_ref = (
        (Path(__file__).parent / "testdata" / "test.yaml.ref")
        .read_text()
        .replace("{%filepath%}", filepath.text())
    )
    assert (
        tmp_ref != Path(save_path.text()).read_text()
    ), "File does not conform with reference"


def test_VerifPage(qtbot, mocker):
    """test VerifPage.

    :param qtbot:
    :param mocker:
    """
    wiz = QWizard()
    mock_path = mocker.patch("QompliGIS.gui_utils.Path")
    mock_yaml = mocker.patch("QompliGIS.gui_utils.yaml")
    mock_doverif = mocker.patch("QompliGIS.gui_utils.DoVerif")
    mock_qgsapp = mocker.patch("QompliGIS.gui_utils.QgsApplication")
    vp = VerifPage()
    wiz.addPage(vp)
    qtbot.addWidget(wiz)
    wiz.show()
    mock_path.assert_called_with(None)
    mock_yaml.full_load.assert_called()
    mock_doverif.assert_called()
    mock_qgsapp.taskManager.assert_called()
    mock_qgsapp.taskManager().addTask.assert_called()


def test_ConfigEditPage(qtbot, tmp_path, mocker):
    """test ConfigEditPage.

    :param qtbot:
    :param tmp_path:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")
    yaml_to_edit = tmp_path / "test_to_edit.yaml"
    yaml_to_edit.write_text(
        (Path(__file__).parent / "testdata" / "test.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}", str(Path(__file__).parent / "testdata" / "shp" / "source")
        )
    )

    save_path = QLineEdit(str(tmp_path / "test.yaml"))
    config_file = QLineEdit(str(tmp_path / "test_to_edit.yaml"))

    cep = ConfigEditPage()
    cep.registerField("save_path", save_path)
    cep.registerField("config_file", config_file)
    wiz = QWizard()
    wiz.addPage(cep)
    qtbot.addWidget(wiz)
    wiz.show()
    # field lat
    cep.stack.currentWidget().tbl_fields.item(4, 1).setCheckState(Qt.Checked)
    # layer cana
    cep.layers_view.setCurrentRow(2)
    cep.stack.currentWidget().chbx_curves_line.setCheckState(Qt.Checked)
    cep.exportConfig()
    message_box.information.assert_called_with(
        None,
        "Completed",
        "The configuration file has been saved to " + save_path.text(),
    )
    tmp_ref = (
        (Path(__file__).parent / "testdata" / "test_edited.yaml.ref")
        .read_text()
        .replace(
            "{%filepath%}", str(Path(__file__).parent / "testdata" / "shp" / "source")
        )
    )
    assert (
        tmp_ref != Path(save_path.text()).read_text()
    ), "File does not conform with reference"


def test_SaveConfigPage(qtbot, mocker):
    """test SaveConfigPage.

    :param qtbot:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")
    config_file = QLineEdit("no_file")

    scp = SaveConfigPage()
    scp.registerField("config_file", config_file)
    wiz = QWizard()
    wiz.addPage(scp)
    qtbot.addWidget(wiz)
    wiz.show()
    assert scp.file_widget.filePath() == "no_file"
    qtbot.keyClick(scp.file_widget.lineEdit(), "a", Qt.ControlModifier)
    qtbot.keyClick(scp.file_widget.lineEdit(), Qt.Key_Delete)
    qtbot.keyClicks(scp.file_widget.lineEdit(), "bad_name.txt")
    scp.validatePage()
    message_box.warning.assert_called_once_with(
        None, "Warning", "The file extension must be .yaml"
    )


def test_SaveReportPage(qtbot, mocker):
    """test SaveReportPage.

    :param qtbot:
    :param mocker:
    """
    message_box = mocker.patch("QompliGIS.gui_utils.QMessageBox")
    report_stub = mocker.stub()
    report_stub.report = mocker.stub()
    mock_path = mocker.patch("QompliGIS.gui_utils.Path")

    srp = SaveReportPage()
    wiz = QWizard()
    wiz.addPage(srp)
    cbx = srp.findChildren(QComboBox)[0]
    report_path = srp.findChildren(QgsFileWidget)[0]
    qtbot.addWidget(wiz)
    wiz.show()

    assert cbx.itemText(0) == ""
    assert cbx.itemText(1) == "Markdown"
    assert cbx.itemText(2) == "HTML"
    assert cbx.itemText(3) == "JSON"
    cbx.setCurrentIndex(3)
    assert report_path.filter() == "JSON (*.json)"
    cbx.setCurrentIndex(2)
    assert report_path.filter() == "HTML (*.html)"
    qtbot.keyClicks(report_path.lineEdit(), "yoyo")
    cbx.setCurrentIndex(1)
    assert report_path.filter() == "Markdown (*.md)"
    srp.save(report_stub)
    report_stub.report.assert_called_with(ReportFormat.MARKDOWN)
    message_box.information.assert_called_once_with(
        None,
        "Finished",
        'The report has correctly been saved to <a href=""'
        + mock_path("yoyo").as_posix()
        + '">yoyo</a>',
    )
